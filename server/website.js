var express = require('express'),
	app = express();
var addApiList= require("./api_routes/apiEngine"),
    conf= require("./conf/config"),
	middleParse = require('./common/middleware.js');

middleParse.middleware(app,express);
addApiList(app);
var srvr=app.listen(conf.development.server.port, function() {
	console.log('listening on port : ' + conf.development.server.port);
});